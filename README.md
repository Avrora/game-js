# Game JS

### __Description du jeu__

Version 0.0.0

Ce jeu est une version du jeu classique [*Arkanoid*](https://fr.wikipedia.org/wiki/Arkanoid).

Dans le jeu, nous avons un plateau, plusieurs blocs et une balle qui doit casser ces blocs.

-----

### **Dans quel partie j'ai réussi**

Le plateau se déplace horizontalement, sans dépasser la page de navigateur. 

La balle est au centre de le plateau. Quand j'appuie sur la touche "Space", la balle commence à bouger.

Approximativement indiqué les limites que la balle ne doit pas franchir.



#### Voici un code en JS:

```
let mirror = document.querySelector(".mirror");
let sun = document.querySelector(".sun");
let cloud = document.querySelector(".cloud");
let page = document.querySelector("body"); //Smena na .background otklychaet vse dvizenie

//Position de sun
moveTop = 0;
moveLeft = -25;


page.addEventListener('keydown', function(event) {
  console.log(event);

  if (event.code == "ArrowLeft" && mirror.offsetLeft > 0) {
    mirror.style.left = mirror.offsetLeft - 20 + "px"; 
  };

 if (event.code == "ArrowRight" && mirror.offsetLeft <= 1270) {
    mirror.style.left = mirror.offsetLeft + 20 + "px";
  }

 if (event.code == "Space") {
    setInterval(function () {
        sun.style.left = sun.offsetLeft + moveTop + "px";
        sun.style.top = sun.offsetTop + moveLeft + "px";  
        borders();
        tapMirror()
    }, 
    30);
  }
});
```

----

### **En version 1.0.0**

La balle traverse le plateau et (après le deuxième mouvement) les frontières.

Je n'ai pas commencé à écrire une fonction pour détruire des blocs.

----

### **L'image du jeu**

![Screenshot_2019-02-20 Game](https://gitlab.com/Avrora/game-js/raw/master/images/Screenshot_Game.png)

### **Fabriqué par**

- HTML
- CSS
- JavaScript